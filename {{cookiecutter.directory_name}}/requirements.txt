Django==3.2.13
django-extensions==3.1.3
django-guardian==2.4.0
acdh-django-browsing==1.0.0
django-spaghetti-and-meatballs==0.4.2
requests>=2.25